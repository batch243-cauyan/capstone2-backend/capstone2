const express = require('express');
const router = express.Router();

const userController = require('../controllers/userController');
const auth = require('../auth');


// endpoint: get All user
router.get('/', userController.getAllUser);

// register a user
router.post('/register', userController.registerUser);


// login
router.post('/login', userController.loginUser);


// toggle admin Role
router.patch('/adminize/:userId', auth.verify,  userController.updateRole)



module.exports = router;
