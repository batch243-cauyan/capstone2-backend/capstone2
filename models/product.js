const mongoose = require('mongoose')

// const Schema = mongoose.Schema

const productSchema = new mongoose.Schema({
    prodName: {
        type:String,
        required:true
    },
    price:{
        type:Number,
        required:true
    },
    description:{
        type:String,
        default: "description"
    },
    category:{
        type:String,
        default:"others"
    },
    specs:{
        dimensions:{
            type:String,
            maxLength: 30,
        },
        colors: [{
            type:String,
            maxLength:30
        }],
        size: [{
            type:String,
            maxLength:10
        }]
    },
    stocks:{
        type:Number,
        required:true
    },
    isAvailable:{
        type:Boolean
    },
    isOnsale:{
        type:Boolean,
        default:false
    }
})


module.exports = mongoose.model("Product", productSchema);