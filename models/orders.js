const mongoose = require('mongoose')


const orderSchema = new mongoose.Schema({
    orderNum:{
        type:Number,
        //must auto Increment
    },
    dateCreated:{
        type:Date,
        default: new Date(),
    },
    products:[{
        productId:{
            type: String,
            required: [true, "productId is required"]
        }
    }],
    status:{
        type:String,
        default: "Pending" 
    }
})

module.exports = mongoose.model('Order', orderSchema);
