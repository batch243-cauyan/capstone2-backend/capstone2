const mongoose = require ('mongoose')

const userSchema = new mongoose.Schema({

        firstName: {
            type: String,
            required: [true, "Firstname is required"]
        },
        middleName: {
            type: String,
        },
        lastName: {
            type: String,
            required: [true, "Firstname is required"]
        },
        isAdmin:{
            type:Boolean,
            default: false
        },
        contacts:{
            mobileNum:{
                type:String
            },
            email:{
                type: String,
                trim: true,
                lowercase: true,
                unique: true,
                validate: {
                    validator: (v)=>{
                        return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(v);
                    },
                    message: "Please enter a valid email"
                },
                required: [true, "Email required"]
            }
        },
        password:{
            type:String,
            minLength: 8,
            required: [true, "Password is required"]
        },
        address:{
          
            province:{
                type:String,
            
                required: [true, "Province is required"]
            },
            municipality: {
                type: String,
                required: [true, "Municipality is required"]
            },
            barangay:{
                type: String,
                required: [true, "Barangay is required"]
            },
            houseNum: {
                type:String,
                required: [true, "Put n/a if not applicable"]
            },
            street:{
                type:String,
                required: [true, "Put n/a if not applicable"]
            },
            zipcode:{
                type: String,
                required: [true, "Zip Code is required"]
            }
        },
        shoppingCart:[{
            productId:{
                type: String,
                required: [true, "productId is required"]
            },
            quantity:{
                type:Number,
                required: [true, "Quantity is required"]
            },
            subTotal:{
                type:Number
            }
        }],
        transactions:[{
            status:{
                type: String,
                required: [true, "transactionId is required"],
            },
            transactionDate:{
                type: Date,
                default: new Date()
            },
            orderId:{
                type: String,
                required:[true, "orderId is required"]
            }
        }]

})


module.exports = mongoose.model("User", userSchema)