const Product = require('../models/product');
const router = require('../routes/productRoutes');


module.exports.addProduct = (requestBody)=>{

    let newProduct = new Product({
        prodName: requestBody.prodName,
        price: requestBody.price,
        description: requestBody.description,
        specs:{
            dimensions:{
                length:requestBody.length,
                width:requestBody.width,
                height:requestBody.height
            },
            color: requestBody.color,
            size: requestBody.size
        },
        stocks:requestBody.stocks,
        isAvailable:requestBody.isAvailable
    })

     return newProduct.save().then((product,error)=>{
        if(error){
            console.log(error);
            return false;
        } else{

           return product; 
        }
    })
    }

module.exports.getAllProducts = ()=>{
    return Product.find({})
    .then(result =>{
        return result})
}
