const User = require('../models/user');
const auth = require('../auth');

const bcrypt = require('bcrypt');




module.exports.registerUser =  async (req,res) =>{
 
      let newUser = new User ({
        firstName:  req.body.firstName,
        middleName: req.body.middleName,
        lastName:   req.body.lastName,
        contacts: {
            mobileNum: req.body.contacts.mobileNum,
            email:     req.body.contacts.email
        },
        password:   bcrypt.hashSync (req.body.password, 10),   
        address:{
            province:     req.body.address.province,
            municipality: req.body.address.municipality,
            barangay:     req.body.address.barangay,
            houseNum:     req.body.address.houseNum,
            street:       req.body.address.street,
            zipcode:      req.body.address.zipcode
           }
        })
    
        console.log(newUser);
        
        return  newUser.save()
        .then(user=>res.status(201).send(`Congratulations, Sir/Ma'am ${newUser.firstName}!. You are now registered.`))
        .catch(error=>{
          console.log(error);
          res.status(400).send(`Sorry ${newUser.firstName}, there was an error during the registration. Please Try again!`)
        })

      
}



module.exports.getAllUser = async (req,res)=>{

    await User.find({}).sort({lastName:1})
    .then(result=>
        {return res.status(200).send(result)}
        )
    .catch(error=>{
        res.status(204).send(`No users found.`)
    })
}



module.exports.loginUser = (req,res) =>{
    
    // if(req.body.email !== null) {
        return User.findOne({email:req.body.email})
        .then((user => {
            if (user === null){
                return res.send(`No registered account under ${req.body.email}. Please Register first.`) 
            }
            else{
       
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, user.password);

                if(isPasswordCorrect){
                    return res.send({accessToken: auth.createAccessToken(user)})
                    // return res.send(`Logged in successfully!`)
                }

                    return res.send(`Incorrect password, please try again!`)
            }

        }))
    // }
}




module.exports.updateRole = async (req,res)=>{

    const token = req.headers.authorization;
    const userData = auth.decode(token);

    let idToBeUdated = req.params.userId

    // let adminStatus = {
    //     isAdmin: req.params.isAdmin
    // }

    if (userData.isAdmin){

        await User.findById(idToBeUdated)
          .then(result=>{
                let update = !result.isAdmin;
            
            return User.findByIdAndUpdate(idToBeUdated, {isAdmin : update}, {new:true})
                    .then(document => {
                        document.password = "Confidential";
                        res.status(200).send(document)})
                    .catch(err => res.send(err))
          })
          .catch(err => res.send(err))

    }
    else{
        return res.send("You don't have access on this page!")
    }

}