
const jwt = require('jsonwebtoken');

const secret = "myShopAPI";


module.exports.createAccessToken = (user) =>{

    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

   return jwt.sign(data, secret, {});

}


module.exports.verify = (req, res, next) =>{
        let token = req.headers.authorization;

        if(token !== undefined ){

            token = token.slice(7, token.length);
            console.log(token)
            return jwt.verify(token, secret, (error,data)=>{
                if(error){
                    return res.send("Invalid Token");
                }
                else{
                    next();
                }
            })
        }

        else{
            return res.send("Authentication failed! No token provided");
        }
}



module.exports.decode = (token) => {
    if (token === undefined){
        return null 
    }
    else{
        token = token.slice(7, token.length);
        return jwt.verify(token, secret, (error, data)=>{
            if (error){
                return null;
            }
            else{
                
                return jwt.decode(token, {complete:true}).payload
            }
        })
    }

}